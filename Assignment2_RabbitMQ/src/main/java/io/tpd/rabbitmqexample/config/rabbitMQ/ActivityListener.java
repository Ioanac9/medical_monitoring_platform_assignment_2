package io.tpd.rabbitmqexample.config.rabbitMQ;

import io.tpd.rabbitmqexample.RabbitmqExampleApplication;
import io.tpd.rabbitmqexample.model.ActivityMessage;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.messaging.simp.SimpMessagingTemplate;
import org.springframework.stereotype.Service;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

@Service
public class ActivityListener {

    @Autowired
    SimpMessagingTemplate template;

    public ActivityMessage mesaj;
    private static final Logger log = LoggerFactory.getLogger(ActivityListener.class);

    @RabbitListener(queues = RabbitmqExampleApplication.DEFAULT_PARSING_QUEUE)
    public void consumeDefaultMessage(ActivityMessage message) {
        log.info("Received activity from RabbitMQ : {}", message.toString());

        mesaj = message;
        String[] values = message.toString().split("[,{}='#]+");

        String id_Pacient = values[2];
        String start = values[4];
        String end = values[6];
        String activity_label = values[8].toString();


        if (activity_label.equals("Sleeping") || activity_label.equals("Leaving") || activity_label.equals("Toileting") || activity_label.equals("Showering")) {
            try {
                DateFormat outf = new SimpleDateFormat("MM-dd-yyyy HH:mm:ss");

                Date dateObj1 = outf.parse(start);
                Date dateObj2 = outf.parse(end);

                long diff = dateObj2.getTime() - dateObj1.getTime();
                if (diff < 0)
                    diff = dateObj1.getTime() - dateObj2.getTime();


                int diffhours = (int) (diff / (60 * 60 * 1000) % 24);
                log.info("difference between hours:{} ", diffhours);

                if ((activity_label.equals("Sleeping")) && (diffhours > 12)) {
                    template.convertAndSend("/topic/greetings", "Pacientul cu id-ul: " + id_Pacient + " doarme de prea mult timp!");
                }

                if ((activity_label.equals("Leaving")) && (diffhours > 12)) {
                    template.convertAndSend("/topic/greetings", "Pacientul cu id-ul: " + id_Pacient + " este iesit de prea mult timp!");
                }

                if ((activity_label.equals("Toileting")) && (diffhours >= 1)) {
                    template.convertAndSend("/topic/greetings", "Pacientul cu id-ul: " + id_Pacient + " sta la baie de prea mult timp!");
                }
                if ((activity_label.equals("Showering")) && (diffhours >= 1)) {
                    template.convertAndSend("/topic/greetings", "Pacientul cu id-ul: " + id_Pacient + " sta la baie de prea mult timp!");
                }
                int diffmin = (int) (diff / (60 * 1000) % 60);
                log.info("difference between minutues:{} ", diffmin);

                int diffsec = (int) (diff / 1000 % 60);
                log.info("difference between seconds:{} ", diffsec);


            } catch (Exception e) {
                e.printStackTrace();
            }

        }
    }
}
