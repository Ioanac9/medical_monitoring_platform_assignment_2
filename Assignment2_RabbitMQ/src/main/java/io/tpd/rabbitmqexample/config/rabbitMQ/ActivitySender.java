package io.tpd.rabbitmqexample.config.rabbitMQ;

import io.tpd.rabbitmqexample.RabbitmqExampleApplication;
import io.tpd.rabbitmqexample.model.ActivityMessage;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;
import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;


@Service
public class ActivitySender {

    private static final Logger log = LoggerFactory.getLogger(ActivitySender.class);

    private final RabbitTemplate rabbitTemplate;
    private final String fileReader = "/D:/1SistemeDistribuiteMateriale/tutorialHelloWorldRabbit/activity.txt";
    private BufferedReader reader;

    public ActivitySender(final RabbitTemplate rabbitTemplate) throws FileNotFoundException {
        this.rabbitTemplate = rabbitTemplate;
        reader = new BufferedReader(new FileReader(fileReader));

    }

    @Scheduled(fixedDelay = 1000L)
    public void sendPracticalPatient() throws IOException {

        String line = reader.readLine();
        if (line != null) {
            System.out.println(line);

            log.info(" Line readed before sent to RabbitMQ:  '" + line);

            String[] values = line.split("\t\t");

            ActivityMessage patient = new ActivityMessage("5c2494a3-1140-4c7a-991a-a1a2561c6bc2",
                    values[0], values[1], values[2]);
            rabbitTemplate.convertAndSend(RabbitmqExampleApplication.EXCHANGE_NAME, RabbitmqExampleApplication.ROUTING_KEY, patient);
            log.info("Activity sent to RabbitMQ " + patient.toString());
        } else {
            reader.close();
            reader = new BufferedReader(new FileReader(fileReader));
        }
    }
}
