package io.tpd.rabbitmqexample.model;

import com.fasterxml.jackson.annotation.JsonProperty;

public class ActivityMessage {

    private String patient_id;
    private String start_time;
    private String end_time;
    private String activity_label;

    public ActivityMessage(@JsonProperty("patient_id") String patient_id,
                           @JsonProperty("start_time") String start_time,
                           @JsonProperty("end_time") String end_time,
                           @JsonProperty("activity_label") String activity_label){
        this.patient_id = patient_id;
        this.start_time = start_time;
        this.end_time = end_time;
        this.activity_label=activity_label;
    }

    public String getPatient_id() {
        return patient_id;
    }

    public void setPatient_id(String patient_id) {
        this.patient_id = patient_id;
    }

    public String getStart_time() {
        return start_time;
    }

    public void setStart_time(String start_time) {
        this.start_time = start_time;
    }

    public String getEnd_time() {
        return end_time;
    }

    public void setEnd_time(String end_time) {
        this.end_time = end_time;
    }

    public String getActivity_label() {
        return activity_label;
    }

    public void setActivity_label(String activity_label) {
        this.activity_label = activity_label;
    }

    @Override
    public String toString() {
        return " : {" +
                "patient_id=" + patient_id +
                ", start_time='" + start_time + '\'' +
                ", end_time='" + end_time + '\'' +
                ", activity_label='" + activity_label + '\'' +
                '}';
    }
}
