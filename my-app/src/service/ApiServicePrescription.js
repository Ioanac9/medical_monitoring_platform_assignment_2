import axios from 'axios';

const PRESCRIPTION_API_BASE_URL = 'http://localhost:8080/prescription';

class ApiServicePrescription {

    fetchPrescriptions() {
        return axios.get(PRESCRIPTION_API_BASE_URL);
    }

    fetchPrescriptionByPatientId(patientId) {
        return axios.get(PRESCRIPTION_API_BASE_URL + '/' + patientId);
    }

    deletePrescription(prescriptionId) {
        return axios.delete(PRESCRIPTION_API_BASE_URL + '/' + prescriptionId);
    }

    addPrescription(prescription) {
        return axios.post(""+PRESCRIPTION_API_BASE_URL, prescription);
    }

    editPrescription(prescription) {
        return axios.put(PRESCRIPTION_API_BASE_URL + '/' + prescription.id, prescription);
    }

}

export default new ApiServicePrescription();