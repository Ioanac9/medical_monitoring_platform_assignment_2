import axios from 'axios';

const CAREGIVER_API_BASE_URL = 'http://localhost:8080/doctor';
const PATIENT_API_BASE_URL = 'http://localhost:8080/doctor';

class ApiServiceDoctor {

    fetchCaregivers() {
        return axios.get(CAREGIVER_API_BASE_URL);
    }

    fetchCaregiverById(caregiverId) {
        return axios.get(CAREGIVER_API_BASE_URL + '/' + caregiverId);
    }

    deleteCaregiver(caregiverId) {
        return axios.delete(CAREGIVER_API_BASE_URL + '/' + caregiverId);
    }

    addCaregiver(caregiver) {
        return axios.post(""+CAREGIVER_API_BASE_URL, caregiver);
    }

    editCaregiver(caregiver) {
        return axios.put(CAREGIVER_API_BASE_URL + '/' + caregiver.id, caregiver);
    }

    fetchPatients() {
        return axios.get(PATIENT_API_BASE_URL);
    }

    fetchPatientById(patientId) {
        return axios.get(PATIENT_API_BASE_URL + '/' + patientId);
    }

    deletePatient(patientId) {
        return axios.delete(PATIENT_API_BASE_URL + '/' + patientId);
    }

    addPatient(patient) {
        return axios.post(""+PATIENT_API_BASE_URL, patient);
    }

    editPatient(patient) {
        return axios.put(PATIENT_API_BASE_URL + '/' + patient.id, patient);
    }

}

export default new ApiServiceDoctor();