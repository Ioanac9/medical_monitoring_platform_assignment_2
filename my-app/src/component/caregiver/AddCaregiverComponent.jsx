import React, { Component } from 'react'
import ApiServiceCaregiver from "../../service/ApiServiceCaregiver";
import TextField from '@material-ui/core/TextField';
import Button from '@material-ui/core/Button';
import Typography from '@material-ui/core/Typography';
import {Input} from "@material-ui/icons";
import Radio from "@material-ui/core/Radio";
import ApiServiceLogin from "../../service/ApiServiceLogin";

class AddCaregiverComponent extends Component{

    constructor(props){
        super(props);
        this.state ={
            firstName: '',
            lastName: '',
            birthDate: '',
            gender: '',
            address: '',
            patients: '',
            username:'',
            password:'',
            role:'',
            message: null
        }
        this.saveCaregiver = this.saveCaregiver.bind(this);
    }

    saveCaregiver = (e) => {
        e.preventDefault();

        let user = {username: this.state.username, password: this.state.password, role: 'caregiver'};
        let caregiver = {firstName: this.state.firstName, lastName: this.state.lastName, birthDate: this.state.birthDate, gender: this.state.gender, address: this.state.address,patients: this.patients,user: user};

        ApiServiceLogin.addUser(user).then(r => {  ApiServiceCaregiver.addCaregiver(caregiver)
            .then(res => {
                this.setState({message : 'Caregiver added successfully.'});
                this.props.history.push('/doctors');
            });});



    }

    onChange = (e) =>
        this.setState({ [e.target.name]: e.target.value });

    render() {
        return(
            <div>
                <Typography variant="h4" style={style}>Add Caregiver</Typography>
                <form style={formContainer}>

                    <TextField type="text" placeholder="Username" fullWidth margin="normal" name="username" value={this.state.username} onChange={this.onChange}/>
                    <TextField type="text" placeholder="Password" fullWidth margin="normal" name="password" value={this.state.password} onChange={this.onChange}/>

                    <TextField type="text" placeholder="First Name" fullWidth margin="normal" name="firstName" value={this.state.firstName} onChange={this.onChange}/>
                    <TextField type="text" placeholder="Last Name" fullWidth margin="normal" name="lastName" value={this.state.lastName} onChange={this.onChange}/>

                    <TextField type="date" placeholder="Birth Date" fullWidth margin="normal" name="birthDate" value={this.state.birthDate} onChange={this.onChange}/>

                    <Radio type = "radio" placeholder="Gender" fullWidth margin="normal" name="gender" value= "Male" onChange={this.onChange}/>Male
                    <Radio type = "radio" placeholder="Gender" fullWidth margin="normal" name="gender" value= "Female" onChange={this.onChange}/>Female

                    <TextField placeholder="Address" fullWidth margin="normal" name="address" value={this.state.address} onChange={this.onChange}/>
                    <Radio type = "radio" placeholder="Patients" fullWidth margin="normal" name="patients" value={this.state.patients}onChange={this.onChange}/>

                    <Button variant="contained" color="primary" onClick={this.saveCaregiver}>Save</Button>
                </form>
            </div>
        );
    }
}
const formContainer = {
    display: 'flex',
    flexFlow: 'row wrap'
};

const style ={
    display: 'flex',
    justifyContent: 'center'

}

export default AddCaregiverComponent;