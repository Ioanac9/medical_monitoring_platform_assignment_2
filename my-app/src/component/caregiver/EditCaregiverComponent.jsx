import React, { Component } from 'react'
import ApiServiceCaregiver from "../../service/ApiServiceCaregiver";
import TextField from '@material-ui/core/TextField';
import Button from '@material-ui/core/Button';
import Typography from '@material-ui/core/Typography';
import Radio from "@material-ui/core/Radio";

class EditCaregiverComponent extends Component {

    constructor(props){
        super(props);
        this.state ={
            id: '',
            firstName: '',
            lastName: '',
            birthDate: '',
            gender: '',
            address: '',
            patients: '',
            message: null
        }
        this.saveCaregiver = this.saveCaregiver.bind(this);
        this.loadCaregiver = this.loadCaregiver.bind(this);
    }

    componentDidMount() {
        this.loadCaregiver();
    }

    loadCaregiver() {
        ApiServiceCaregiver.fetchCaregiverById(window.localStorage.getItem("caregiverId"))
            .then((res) => {
                let caregiver = res.data.result;
                this.setState({
                    id: caregiver.id,
                    firstName: caregiver.firstName,
                    lastName: caregiver.lastName,
                    birthDate: caregiver.birthDate,
                    gender: caregiver.gender,
                    address: caregiver.address,
                    patients: caregiver.patients
                })
            });
    }

    onChange = (e) =>
        this.setState({ [e.target.name]: e.target.value });

    saveCaregiver = (e) => {
        e.preventDefault();
        let caregiver = {id: this.state.id,firstName: this.state.firstName,lastName: this.state.lastName ,birthDate: this.state.birthDate, gender: this.state.gender, address: this.state.address,patients: this.state.patients};
        ApiServiceCaregiver.editCaregiver(caregiver)
            .then(res => {
                this.setState({message : 'Caregiver added successfully.'});
                this.props.history.push('/doctors');
            });
    }

    render() {
        return (
            <div>
                <Typography variant="h4" style={style}>Edit Caregiver</Typography>
                <form>

                    <TextField type="text" placeholder="First Name" fullWidth margin="normal" name="firstName" value={this.state.firstName} onChange={this.onChange}/>
                    <TextField type="text" placeholder="Last Name" fullWidth margin="normal" name="lastName" value={this.state.lastName} onChange={this.onChange}/>

                    <TextField type="date" placeholder="Birth Date" fullWidth margin="normal" name="birthDate" value={this.state.birthDate} onChange={this.onChange}/>

                    <Radio type = "radio" placeholder="Gender" fullWidth margin="normal" name="gender" value= "Male" onChange={this.onChange}/>Male
                    <Radio type = "radio" placeholder="Gender" fullWidth margin="normal" name="gender" value= "Female" onChange={this.onChange}/>Female

                    <TextField placeholder="Address" fullWidth margin="normal" name="address" value={this.state.address} onChange={this.onChange}/>

                    <Radio type = "radio" placeholder="Patients" fullWidth margin="normal" name="patients" value= {this.state.patients} onChange={this.onChange}/>
                    <Button variant="contained" color="primary" onClick={this.saveCaregiver}>Edit</Button>

                </form>
            </div>
        );
    }
}

const style ={
    display: 'flex',
    justifyContent: 'center'
}

export default EditCaregiverComponent;