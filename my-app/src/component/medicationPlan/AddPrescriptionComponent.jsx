import React, { Component } from 'react'
import TextField from '@material-ui/core/TextField';
import Button from '@material-ui/core/Button';
import Typography from '@material-ui/core/Typography';
import {Input} from "@material-ui/icons";
import Radio from "@material-ui/core/Radio";
import Table from "@material-ui/core/Table";
import TableHead from "@material-ui/core/TableHead";
import TableRow from "@material-ui/core/TableRow";
import TableCell from "@material-ui/core/TableCell";
import TableBody from "@material-ui/core/TableBody";
import CreateIcon from "@material-ui/core/SvgIcon/SvgIcon";
import ApiServicePrescription from "../../service/ApiServicePrescription";
import ApiServiceCaregiver from "../../service/ApiServiceCaregiver";
import ApiServiceMedication from "../../service/ApiServiceMedication";
import ApiServicePatient from "../../service/ApiServicePatient";

class AddPrescriptionComponent extends Component{

    constructor(props){
        super(props);
        this.state ={
            id:'',
            name:'',
            sideEffects:'',
            dosage:'',
            intokeInterval:'',
            periodTreatment:'',
            patientId:'',
            medicationId:'',
            message: null
        }
       // this.savePrescription = this.savePrescription.bind(this);
        //this.loadPrescription = this.loadPrescription.bind(this);
        this.loadMedication= this.loadMedication.bind(this);

    }
    componentDidMount() {
        this.loadMedication();
    }

    loadMedication() {
        ApiServiceMedication.fetchMedicationById(window.localStorage.getItem("medicationIdForPrescription"))
            .then((res) => {
                let medication = res.data.result;
                this.setState({
                    id: medication.id,
                    name: medication.name,
                    sideEffects: medication.sideEffects,
                    dosage: medication.dosage
                })
            });
    }

    //
    // componentDidMount() {
    //     this.loadPrescription();
    //
    // }
    //
    // loadPrescription() {
    //     ApiServicePrescription. fetchPrescriptionByPatientId()
    //         .then((res) => {
    //             let prescription= res.data.result;
    //             this.setState({
    //                 id:prescription.id,
    //                 firstName:prescription.firstName,
    //                 lastName:prescription.lastName,
    //                 birthDate: prescription.birthDate,
    //                 gender: prescription.gender,
    //                 address: prescription.address,
    //                 medicalRecord: prescription.medicalRecord
    //
    //             })
    //
    //         });
    // }

    savePrescription = (e) => {
        e.preventDefault();

        let prescription ={intokeInterval: this.state.intokeInterval,periodTreatment: this.state.periodTreatment,
                        medicationId:window.localStorage.getItem("medicationIdForPrescription"),
                        patientId: window.localStorage.getItem("medicationPlanPatientId")};


        ApiServicePrescription.addPrescription(prescription)
            .then(res => {

                this.setState({message : 'Medication plan added successfully.'});
                this.props.history.push('/doctors');
            });
        // let medication = {id: this.state.id,name: this.state.name,sideEffects: this.state.sideEffects ,
        //     dosage: this.state.dosage,patient: window.localStorage.getItem("medicationPlanPatientId")};
        // ApiServiceMedication.editMedication(medication)
        //     .then(res => {
        //         this.setState({message : 'Medication added successfully.'});
        //         this.props.history.push('/medications');
        //     });
        let medication = {id: this.state.id,name: this.state.name,sideEffects: this.state.sideEffects ,dosage: this.state.dosage,patient: null,
        patientId: window.localStorage.getItem("medicationPlanPatientId")};
        ApiServiceMedication.editMedication(medication)
            .then(res => {
                this.setState({message : 'Medication added successfully.'});
                this.props.history.push('/medications');
            });
    }


    onChange = (e) =>
        this.setState({ [e.target.name]: e.target.value });


    // render() {
    //     return(
    //         <div>
    //             <Typography variant="h4" style={style}>Add Medical Plan</Typography>
    //             <form style={formContainer}>
    //
    //                 <Button variant="contained" color="primary" onClick={this.savePrescription}>Save</Button>
    //             </form>
    //         </div>
    //     );
    // }



    render() {
        return(
            <div>
                <Typography variant="h4" style={style}>Add Prescription</Typography>
                <form style={formContainer}>

                    <TextField type="number" placeholder="Intoke Interval " fullWidth margin="normal" name="intokeInterval" value={this.state.intokeInterval} onChange={this.onChange}/>

                    <TextField type="number" placeholder="Period Treatment" fullWidth margin="normal" name="periodTreatment" value={this.state.periodTreatment} onChange={this.onChange}/>

                    <Button variant="contained" color="primary" onClick={this.savePrescription}>Save Prescription</Button>
                </form>
            </div>
        );
    }

}
const formContainer = {
    display: 'flex',
    flexFlow: 'row wrap'
};

const style ={
    display: 'flex',
    justifyContent: 'center'

}

export default AddPrescriptionComponent;