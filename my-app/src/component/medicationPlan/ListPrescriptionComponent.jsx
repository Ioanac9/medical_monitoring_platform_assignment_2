import React, { Component } from 'react'
import ApiServicePrescription from "../../service/ApiServicePrescription";
import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableHead from '@material-ui/core/TableHead';
import TableRow from '@material-ui/core/TableRow';
import Button from '@material-ui/core/Button';
import CreateIcon from '@material-ui/icons/Create';
import DeleteIcon from '@material-ui/icons/Delete';
import Typography from '@material-ui/core/Typography';
import ApiServiceMedication from "../../service/ApiServiceMedication";

class ListPrescriptionComponent extends Component {

    constructor(props) {
        super(props)
        this.state = {
            medications: [],
            message: null
        }
        this.deletePrescription = this.deletePrescription.bind(this);
        this.editPrescription = this.editPrescription.bind(this);
       // this.addPrescription = this.addPrescription.bind(this);
        this.reloadPrescriptionList = this.reloadPrescriptionList.bind(this);
    }

    componentDidMount() {
        this.reloadPrescriptionList();
    }

    reloadPrescriptionList() {
        ApiServiceMedication.fetchMedications()
            .then((res) => {
                this.setState({medications: res.data.result})
            });
    }

    deletePrescription(prescriptionId) {
        ApiServicePrescription.deletePrescription(prescriptionId)
            .then(res => {
                this.setState({message : 'Prescription deleted successfully.'});
                this.setState({prescriptions: this.state.prescriptions.filter(prescription => prescription.id !== prescriptionId)});
            })
    }

    editPrescription(id) {
        window.localStorage.setItem("prescriptionId", id);
        this.props.history.push('/edit-prescription');
    }
//////////////////////////////////////////
    addPrescription(medicationId) {
        window.localStorage.setItem("medicationIdForPrescription", medicationId);
        window.localStorage.removeItem("prescriptionId");
         this.props.history.push('/add-prescription');
    }

    render() {
        return (
            <div>
                <Typography variant="h4" style={style}>Prescription Details</Typography>

                <Table>
                    <TableHead>
                        <TableRow>
                            <TableCell>Id</TableCell>
                            <TableCell>Name</TableCell>
                            <TableCell align="right">Side Effects</TableCell>
                            <TableCell align="right">Dosage</TableCell>
                        </TableRow>
                    </TableHead>
                    <TableBody>
                        {this.state.medications.map(row => (
                            <TableRow key={row.id}>
                                <TableCell component="th" scope="row">
                                    {row.id}
                                </TableCell>
                                <TableCell align="right">{row.name}</TableCell>
                                <TableCell align="right">{row.sideEffects}</TableCell>
                                <TableCell align="right">{row.dosage}</TableCell>

                                <TableCell align="right" onClick={() => this.editPrescription(row.id)}><CreateIcon /></TableCell>
                                <TableCell align="right" onClick={() => this.deletePrescription(row.id)}><DeleteIcon /></TableCell>

                                <Button variant="contained" color="primary" onClick={() => this.addPrescription(row.id)}>
                                    Add Prescription
                                </Button>

                            </TableRow>
                        ))}
                    </TableBody>
                </Table>

            </div>
        );
    }

}

const style ={
    display: 'flex',
    justifyContent: 'center'
}

export default ListPrescriptionComponent;