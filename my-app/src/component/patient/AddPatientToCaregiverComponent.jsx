import React, { Component } from 'react'
import ApiServicePatient from "../../service/ApiServicePatient";
import ApiServiceCaregiver from "../../service/ApiServiceCaregiver";
import TextField from '@material-ui/core/TextField';
import Button from '@material-ui/core/Button';
import Typography from '@material-ui/core/Typography';
import {Input} from "@material-ui/icons";
import Radio from "@material-ui/core/Radio";
import Table from "@material-ui/core/Table";
import TableHead from "@material-ui/core/TableHead";
import TableRow from "@material-ui/core/TableRow";
import TableCell from "@material-ui/core/TableCell";
import TableBody from "@material-ui/core/TableBody";
import CreateIcon from "@material-ui/core/SvgIcon/SvgIcon";
import ApiServiceLogin from "../../service/ApiServiceLogin";

class AddPatientToCaregiverComponent extends Component{

    constructor(props){
        super(props);
        this.state ={
            id:'',
            firstName: '',
            lastName: '',
            birthDate: '',
            gender: '',
            address: '',
            patients: [],
            medicalRecord: '',
            Caregiver_idCaregiver:'',
            username:'',
            password:'',
            role:'',
            message: null
        }
        this.savePatient = this.savePatient.bind(this);
        this.loadCaregiver = this.loadCaregiver.bind(this);


    }

    componentDidMount() {
        this.loadCaregiver();

    }

    loadCaregiver() {
        ApiServiceCaregiver.fetchCaregiverById(window.localStorage.getItem("patientToCaregiverId"))
            .then((res) => {
                let caregiver= res.data.result;
                this.setState({
                    caregiverId:caregiver.id,
                    caregiverFirstName:caregiver.firstName,
                    lastName:caregiver.lastName,
                    birthDate: caregiver.birthDate,
                    gender: caregiver.gender,
                    address: caregiver.address,
                    patients: caregiver.patients

                })

            });
    }


    savePatient () {

        let user = {username: this.state.username, password: this.state.password, role: 'patient'};
        let caregiver ={id: this.state.id,firstName:this.state.firstName,lastName:this.state.lastName,
            birthDate:this.state.birthDate,gender:this.state.gender,address:this.state.address,
            caregiverId :window.localStorage.getItem("patientToCaregiverId"),
            caregiverFirstName:'', caregiverLastName:'', caregiverGender:'', caregiverAddress:'',
            userUsername: this.state.username};
        window.localStorage.setItem("caregiverId", caregiver.id);
        window.localStorage.setItem("caregiverFirstName", caregiver.firstName);
        window.localStorage.setItem("caregiverLastName", caregiver.lastName);
        ApiServiceLogin.addUser(user).then(r => {

            ApiServicePatient.addPatient(caregiver)
                .then(res => {
                    this.setState({message : 'Patient added successfully.'});
                    this.props.history.push('/doctors');
                });

        });


        // let patient = {medicalRecord: this.state.medicalRecord,Caregiver_idCaregiver: caregiver };
        // ApiServicePatient.addPatient(patient)
        //     .then(res => {
        //         this.setState({message: 'Patient added successfully.'});
        //         this.props.history.push('/patients');
        //     });

    }

    onChange = (e) =>
        this.setState({ [e.target.name]: e.target.value });

    // render() {
    //     return (
    //         <div>
    //             <Typography variant="h4" style={style}>Patient Details</Typography>
    //             <Button variant="contained" color="primary" onClick={() => this.addPatient()}>
    //                 Add Patient
    //             </Button>
    //
    //             <Table>
    //                 <TableHead>
    //                     <TableRow>
    //                         <TableCell>Id</TableCell>
    //                         <TableCell>First Name</TableCell>
    //                         <TableCell align="right">Last Date</TableCell>
    //                         <TableCell align="right">Birth Date</TableCell>
    //                         <TableCell align="right">Gender</TableCell>
    //                         <TableCell align="right">Address</TableCell>
    //
    //                     </TableRow>
    //                 </TableHead>
    //                 <TableBody>
    //
    //
    //                             <TableCell align="right">{this.state.firstName}</TableCell>
    //                             <TableCell align="right">{this.state.lastName}</TableCell>
    //                             <TableCell align="right">{this.state.birthDate}</TableCell>
    //                             <TableCell align="right">{this.state.gender}</TableCell>
    //                             <TableCell align="right">{this.state.address}</TableCell>
    //
    //
    //
    //
    //                 </TableBody>
    //             </Table>
    //
    //         </div>
    //     );
    // }


    // render() {
    //     return(
    //         <div>
    //             <Typography variant="h4" style={style}>Add Patient</Typography>
    //             <form style={formContainer}>
    //
    //                 <Button variant="contained" color="primary" onClick={this.savePatient}>Save</Button>
    //             </form>
    //         </div>
    //     );
    // }

    render() {
        return(
            <div>
                <Typography variant="h4" style={style}>Add Patient PLEASEEE</Typography>
                <form style={formContainer}>

                    <TextField type="text" placeholder="Username" fullWidth margin="normal" name="username" value={this.state.username} onChange={this.onChange}/>
                    <TextField type="text" placeholder="Password" fullWidth margin="normal" name="password" value={this.state.password} onChange={this.onChange}/>

                    <TextField type="text" placeholder="First Name" fullWidth margin="normal" name="firstName" value={this.state.firstName} onChange={this.onChange}/>
                    <TextField type="text" placeholder="Last Name" fullWidth margin="normal" name="lastName" value={this.state.lastName} onChange={this.onChange}/>

                    <TextField type="date" placeholder="Birth Date" fullWidth margin="normal" name="birthDate" value={this.state.birthDate} onChange={this.onChange}/>

                    <Radio type = "radio" placeholder="Gender" fullWidth margin="normal" name="gender" value= "Male" onChange={this.onChange}/>Male
                    <Radio type = "radio" placeholder="Gender" fullWidth margin="normal" name="gender" value= "Female" onChange={this.onChange}/>Female

                    <TextField placeholder="Address" fullWidth margin="normal" name="address" value={this.state.address} onChange={this.onChange}/>

                    <TextField placeholder="Medical Record" fullWidth margin="normal" name="medicalRecord" value={this.state.medicalRecord} onChange={this.onChange}/>

                    <Button variant="contained" color="primary" onClick={this.savePatient}>Save</Button>
                </form>
            </div>
        );
    }
}
const formContainer = {
    display: 'flex',
    flexFlow: 'row wrap'
};

const style ={
    display: 'flex',
    justifyContent: 'center'

}

export default AddPatientToCaregiverComponent;