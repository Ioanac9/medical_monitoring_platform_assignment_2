import React, { Component } from 'react'
import ApiServicePatient from "../../service/ApiServicePatient";
import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableHead from '@material-ui/core/TableHead';
import TableRow from '@material-ui/core/TableRow';
import Button from '@material-ui/core/Button';
import CreateIcon from '@material-ui/icons/Create';
import DeleteIcon from '@material-ui/icons/Delete';
import Typography from '@material-ui/core/Typography';
import ApiServiceLogin from "../../service/ApiServiceLogin";

class ListPatientComponent extends Component {

    constructor(props) {
        super(props)
        this.state = {
            users:[],
            patients: [],
            message: null
        }
        this.deletePatient = this.deletePatient.bind(this);
        this.editPatient = this.editPatient.bind(this);
        this.addPatient = this.addPatient.bind(this);
        this.reloadPatientList = this.reloadPatientList.bind(this);
    }

    componentDidMount() {
        this.reloadPatientList();
    }

    reloadPatientList() {
        ApiServicePatient.fetchPatients()
            .then((res) => {
                ApiServiceLogin.fetchUsers().then((r)=>
                {
                this.setState({users :r.data.result})
                });
                this.setState({patients: res.data.result})
            });
    }

    deletePatient(patientId) {
        ApiServicePatient.deletePatient(patientId)
            .then(res => {
                this.setState({message : 'Patient deleted successfully.'});
                this.setState({patients: this.state.patients.filter(patient => patient.id !== patientId)});
            })
    }

    editPatient(id) {
        window.localStorage.setItem("patientId", id);
        this.props.history.push('/edit-patient');
    }

    addPatient() {
        window.localStorage.removeItem("patientId");
        this.props.history.push('/add-patient');
    }

    render() {
        return (
            <div>
                <Typography variant="h4" style={style}>Patient Details</Typography>
                <Button variant="contained" color="primary" onClick={() => this.addPatient()}>
                    Add Patient
                </Button>

                <Table>
                    <TableHead>
                        <TableRow>
                            <TableCell>Id</TableCell>
                            <TableCell align="right">Username</TableCell>
                            <TableCell align="right">Password</TableCell>
                            <TableCell align="right">First Name</TableCell>
                            <TableCell align="right">Last Date</TableCell>
                            <TableCell align="right">Birth Date</TableCell>
                            <TableCell align="right">Gender</TableCell>
                            <TableCell align="right">Address</TableCell>
                            <TableCell align="right">Medical Record</TableCell>
                        </TableRow>
                    </TableHead>
                    <TableBody>
                        {this.state.patients.map(row => (
                            <TableRow key={row.id}>
                                <TableCell component="th" scope="row">
                                    {row.id}
                                </TableCell>
                                <TableCell align="right">{row.username}</TableCell>
                                <TableCell align="right">{row.password}</TableCell>
                                <TableCell align="right">{row.firstName}</TableCell>
                                <TableCell align="right">{row.lastName}</TableCell>
                                <TableCell align="right">{row.birthDate}</TableCell>
                                <TableCell align="right">{row.gender}</TableCell>
                                <TableCell align="right">{row.address}</TableCell>
                                <TableCell align="right">{row.medicalRecord}</TableCell>

                                <TableCell align="right" onClick={() => this.editPatient(row.id)}><CreateIcon /></TableCell>
                                <TableCell align="right" onClick={() => this.deletePatient(row.id)}><DeleteIcon /></TableCell>

                            </TableRow>
                        ))}
                    </TableBody>
                </Table>

            </div>
        );
    }

}

const style ={
    display: 'flex',
    justifyContent: 'center'
}

export default ListPatientComponent;