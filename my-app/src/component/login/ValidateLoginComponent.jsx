
import { FormGroup, FormControl, FormLabel } from "react-bootstrap";
import "./Login.css";
import React, { Component } from 'react'
import TextField from '@material-ui/core/TextField';
import Button from '@material-ui/core/Button';
import ApiServiceLogin from "../../service/ApiServiceLogin";

import Radio from "@material-ui/core/Radio";
import Table from "@material-ui/core/Table";
import TableHead from "@material-ui/core/TableHead";
import TableRow from "@material-ui/core/TableRow";
import TableCell from "@material-ui/core/TableCell";
import TableBody from "@material-ui/core/TableBody";
import Typography from '@material-ui/core/Typography';
import DeleteIcon from "@material-ui/core/SvgIcon/SvgIcon";
import ApiServiceMedication from "../../service/ApiServiceMedication";
import ApiServiceCaregiver from "../../service/ApiServiceCaregiver";
import Redirect from "react-router-dom/es/Redirect";


class LoginFormComponent extends Component {


    constructor(props) {
        super(props)
        this.state = {
            username: '',
            password: '',
            role: '',
            idCaregiver:'',
            idPatient:'',
            id:'',
            firstName: '',
            lastName: '',
            birthDate: '',
            gender: '',
            address: '',
            patients: [],
            message: null
        }
        this.validate= this.validate.bind(this);
    }


    componentDidMount(){
        this.validate()
    }

    validate(){

        ApiServiceLogin.fetchCaregiverByUsername( window.localStorage.getItem("usernameLogin"), window.localStorage.getItem("passwordLogin")) .then((res) => {
            let users = res.data.result;

            this.setState({

                username: users.username,
                password: users.password,
                id: window.localStorage.setItem("idLogin",users.id),
                role: window.localStorage.setItem("roleLogin",users.role),
                idCaregiver: window.localStorage.setItem("idCaregiverLogin",users.idCaregiver),
                idPatient: window.localStorage.setItem("idPatientLogin",users.idPatient)

            })
        });



    }

    show(){

       // if(this.state.id !== '') {
            if (window.localStorage.getItem("roleLogin") === 'patient') {

                this.props.history.push('/patient-view');
            } else {
                if (window.localStorage.getItem("roleLogin") === 'caregiver') {
                   this.props.history.push('caregiver-view');
                }
                else{ if (window.localStorage.getItem("roleLogin") === 'doctor') {
                    this.props.history.push('doctors');
                }

                }
            }
      //  }

    }


    redirect(){
        this.props.history.push('/add-medication');


    }


    noOp(){
        this.props.history.push('/login');


    }

    onChange = (e) =>
        this.setState({[e.target.name]: e.target.value});

    render()
    {
        return (
            <div>

                <Typography variant="h4" >User Details</Typography>
                <Button variant="contained" color="primary" onClick={() => this.show()}>
                    View
                </Button>

                <Table>
                    <TableHead>
                        <TableRow>
                            <TableCell align="right">Id</TableCell>
                            <TableCell align="right">User Name</TableCell>
                            <TableCell align="right">Password</TableCell>
                            <TableCell align="right">Role</TableCell>


                        </TableRow>
                    </TableHead>
                    <TableBody>

                        <TableCell align="right"> {window.localStorage.getItem("idLogin")}</TableCell>
                        <TableCell align="right">{this.state.username}</TableCell>
                        <TableCell align="right">{this.state.password}</TableCell>
                        <TableCell align="right"> {window.localStorage.getItem("roleLogin")}</TableCell>


                    </TableBody>
                </Table>

            </div>
        );

    }
}

export default LoginFormComponent;