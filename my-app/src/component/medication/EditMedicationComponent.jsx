import React, { Component } from 'react'
import ApiServiceMedication from "../../service/ApiServiceMedication";
import TextField from '@material-ui/core/TextField';
import Button from '@material-ui/core/Button';
import Typography from '@material-ui/core/Typography';
import Radio from "@material-ui/core/Radio";

class EditMedicationComponent extends Component {

    constructor(props){
        super(props);
        this.state ={
            id: '',
            name: '',
            sideEffects: '',
            dosage: '',
            message: null
        }
        this.saveMedication = this.saveMedication.bind(this);
        this.loadMedication = this.loadMedication.bind(this);
    }

    componentDidMount() {
        this.loadMedication();
    }

    loadMedication() {
        ApiServiceMedication.fetchMedicationById(window.localStorage.getItem("medicationId"))
            .then((res) => {
                let medication = res.data.result;
                this.setState({
                    id: medication.id,
                    name: medication.name,
                    sideEffects: medication.sideEffects,
                    dosage: medication.dosage
                })
            });
    }

    onChange = (e) =>
        this.setState({ [e.target.name]: e.target.value });

    saveMedication = (e) => {
        e.preventDefault();
        let medication = {id: this.state.id,name: this.state.name,sideEffects: this.state.sideEffects ,dosage: this.state.dosage};
        ApiServiceMedication.editMedication(medication)
            .then(res => {
                this.setState({message : 'Medication added successfully.'});
                this.props.history.push('/doctors');
            });
    }

    render() {
        return (
            <div>
                <Typography variant="h4" style={style}>Edit Medication</Typography>
                <form>

                    <TextField type="text" placeholder="Name" fullWidth margin="normal" name="name" value={this.state.name} onChange={this.onChange}/>
                    <TextField type="text" placeholder="Side Effects" fullWidth margin="normal" name="sideEffects" value={this.state.sideEffects} onChange={this.onChange}/>
                    <TextField type="number" placeholder="Dosage" fullWidth margin="normal" name="dosage" value={this.state.dosage} onChange={this.onChange}/>
                    <Button variant="contained" color="primary" onClick={this.saveMedication}>Edit</Button>

                </form>
            </div>
        );
    }
}

const style ={
    display: 'flex',
    justifyContent: 'center'
}

export default EditMedicationComponent;