import React, { Component } from 'react'
import ApiServiceMedication from "../../service/ApiServiceMedication";
import TextField from '@material-ui/core/TextField';
import Button from '@material-ui/core/Button';
import Typography from '@material-ui/core/Typography';
import {Input} from "@material-ui/icons";
import Radio from "@material-ui/core/Radio";

class AddMedicationComponent extends Component{

    constructor(props){
        super(props);
        this.state ={
            name: '',
            sideEffects: '',
            dosage: '',
            message: null
        }
        this.saveMedication = this.saveMedication.bind(this);
    }

    saveMedication = (e) => {
        e.preventDefault();
        let medication = {name: this.state.name, sideEffects: this.state.sideEffects, dosage: this.state.dosage};
        ApiServiceMedication.addMedication(medication)
            .then(res => {
                this.setState({message : 'Medication added successfully.'});
                this.props.history.push('/doctors');
            });
    }

    onChange = (e) =>
        this.setState({ [e.target.name]: e.target.value });

    render() {
        return(
            <div>
                <Typography variant="h4" style={style}>Add Medication</Typography>
                <form style={formContainer}>

                    <TextField type="text" placeholder="Name" fullWidth margin="normal" name="name" value={this.state.name} onChange={this.onChange}/>
                    <TextField type="text" placeholder="Side Effects" fullWidth margin="normal" name="sideEffects" value={this.state.sideEffects} onChange={this.onChange}/>
                    <TextField type="number" placeholder="Dosage" fullWidth margin="normal" name="dosage" value={this.state.dosage} onChange={this.onChange}/>
                    <Button variant="contained" color="primary" onClick={this.saveMedication}>Save</Button>
                </form>
            </div>
        );
    }
}
const formContainer = {
    display: 'flex',
    flexFlow: 'row wrap'
};

const style ={
    display: 'flex',
    justifyContent: 'center'

}

export default AddMedicationComponent;