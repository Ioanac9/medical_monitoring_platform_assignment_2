import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import App from './App';
import * as serviceWorker from './serviceWorker';

// import "./styles.css";
// import ValidatedLoginForm from "./ValidatedLoginForm";
//
// function App() {
//     return (
//         <div className="App">
//             <h1>Validated Login Form</h1>
//             <ValidatedLoginForm />
//         </div>
//     );
// }


const Koa = require('koa');
const app = new Koa();

// cors middleware
app.use(async (ctx, next) => {
    const origin = ctx.get('Origin');
    if (ctx.method !== 'OPTIONS') {
        ctx.set('Access-Control-Allow-Origin', 'http://localhost:8091/');
        ctx.set('Access-Control-Allow-Credentials', 'true');
    } else if (ctx.get('Access-Control-Request-Method')) {
        ctx.set('Access-Control-Allow-Origin','http://localhost:8091/');
        ctx.set('Access-Control-Allow-Methods', 'GET');
        ctx.set('Access-Control-Allow-Headers', 'Content-Type');
        ctx.set('Access-Control-Max-Age', '42');
        ctx.set('Access-Control-Allow-Credentials', 'true');
    }
    await next();
});

app.use(async ctx => {
    ctx.body = '"Hello server"';
});


ReactDOM.render(<App />, document.getElementById('root'));

// If you want your app to work offline and load faster, you can change
// unregister() to register() below. Note this comes with some pitfalls.
// Learn more about service workers: https://bit.ly/CRA-PWA
// serviceWorker.unregister();
