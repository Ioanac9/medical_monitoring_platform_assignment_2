import React from "react";
import Button from "@material-ui/core/Button";
import ApiServiceLogin from "./service/ApiServiceLogin";

export class LoginForm extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            user: "",
            username: "",
            password: ""
        };
    }

    loginUser(username,password){

        ApiServiceLogin.fetchCaregiverByUsername(username,password)
            .then((res) => {
                this.setState({user: res.data.result})
            });

        window.localStorage.setItem("usernameLocal", username);
        window.localStorage.setItem("passwordLocal", password);
        //this.props.history.push('/one-user');

    }

    render() {
        const { username, password } = this.state;
        return (
            <form onSubmit={this.handleSubmit}>
                <label htmlFor="username">Email</label>
                <input
                    name="username"
                    type="text"
                    placeholder="Enter your username"
                    value={username}
                    onChange={this.handleChange}
                />
                <label htmlFor="username">Password</label>
                <input
                    name="password"
                    type="password"
                    placeholder="Enter your password"
                    value={password}
                    onChange={this.handleChange}
                />
                <Button variant="contained" color="primary" onClick={() => this.loginUser(this.state.username,this.state.password)}>
                    Login
                </Button>
            </form>
        );
    }

    handleChange = event => {
        this.setState({
            [event.target.name]: event.target.value
        });
    };

    handleSubmit = event => {
        console.log("Submitting");
        console.log(this.state);
    };
}
